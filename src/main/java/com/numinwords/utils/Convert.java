package com.numinwords.utils;

public class Convert {

	// function convert decimal to octal

	public static long getOctal(long number) {

		return Long.valueOf(Long.toOctalString(Math.abs(number)));
	}

}
