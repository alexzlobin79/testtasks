package com.numinwords.project.service;

import java.util.HashMap;
import java.util.Map;

import com.numinwords.project.api.INumberSystem;
import com.numinwords.project.convert.ConvertNumberInRuWords;
import com.numinwords.project.model.NumWords;
import com.numinwords.project.model.Words;
import com.numinwords.utils.Convert;

public class NumWordsService {

	private static final short NUM_LANG = 2;

	static public Words getWordsfromNumber(long number) {
		String numRuDec = ConvertNumberInRuWords.convertToWords(number);
		String numRuOct = ConvertNumberInRuWords.convertToWords(Convert.getOctal(number));
		// English don't implementation!!! This is test project!!!
		String[] arr = { numRuDec, numRuOct, "ENG10XXX", "ENG8XXX" };
		return fillData(arr);
	}

	// Fill Words data use array
	static private Words fillData(String[] arr) {

		Map<String, String> en_words = new HashMap<>(NUM_LANG);
		Map<String, String> ru_words = new HashMap<>(NUM_LANG);
		ru_words.put(INumberSystem.DECIMAL, arr[0]);
		ru_words.put(INumberSystem.OCTAL, arr[1]);
		en_words.put(INumberSystem.DECIMAL, arr[2]);
		en_words.put(INumberSystem.OCTAL, arr[3]);
		NumWords res = new NumWords();
		res.setEn(en_words);
		res.setRu(ru_words);
		return (new Words(res));
	}

}
