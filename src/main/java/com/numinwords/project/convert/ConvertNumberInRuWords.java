package com.numinwords.project.convert;

public class ConvertNumberInRuWords {
	// Define digits in female/male

	private static final String namesFirstTens[][] = {
			{ "одна", "две", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять" }, { "один", "два" } };
	private static final String namesSecondTens[] = { "десять", "одиннадцать", "двенадцать", "тринадцать",
			"четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать" };
	private static final String namesOtherTens[] = { "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят",
			"семьдесят", "восемьдесят", "девяносто" };
	private static final String namesHundreds[] = { "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот",
			"семьсот", "восемьсот", "девятьсот" };
	private static final String nameThousand[] = { "тысяча", "тысячи", "тысяч" };
	private static final String nameMillion[] = { "миллион", "миллиона", "миллионов" };
	private static final String nameBillion[] = { "миллиард", "миллиарда", "миллиардов" };
	private static final String nameTrillion[] = { "триллион", "триллиона", "триллионов" };

	private static final int TEN = 10;
	private static final int HUNDRED = 100;
	private static final int THOUSAND = 1000;
	private static final long TRILLION = (long) 1E12;

	private static int[] getDigitsNumberThreeDigit(int number) {

		int hundreds = number / HUNDRED;// get numbers of hundreds
		int remNumHundred = number % HUNDRED;// get number minus hundreds
		int decimals = remNumHundred / TEN;// get numbers of decimals
		int units = remNumHundred % TEN;// get numbers of units
		int[] digits = { hundreds, decimals, units };
		return digits;
	}

	// Function convert numbers (in range from 0 to 999) in words
	private static String convertNumberThreeDigitInWords(short number, Gender gender)

	{

		if (number < 0 || number > 999)
			throw new IllegalArgumentException("Number out of range(0-999)");

		if (number == 0)
			return "ноль";

		StringBuilder result = new StringBuilder(100);

		int[] digits = getDigitsNumberThreeDigit(number);

		// add word of hundreds

		if (digits[0] != 0)
			result.append(" ").append(namesHundreds[digits[0] - 1]);

		// add word of decimal

		switch (digits[1]) {
		case 0:
			break;
		case 1: {
			result.append(" ").append(namesSecondTens[digits[2]]);
			return result.toString().trim();
		}
		default:
			result.append(" ").append(namesOtherTens[digits[1] - 2]);
		}

		// add word of units
		switch (digits[2]) {
		case 0:
			break;
		case 1:
			result.append(" ").append(namesFirstTens[gender.ordinal()][0]);
			break;
		case 2:
			result.append(" ").append(namesFirstTens[gender.ordinal()][1]);
			break;
		default:
			result.append(" ").append(namesFirstTens[0][digits[2] - 1]);
		}

		return result.toString().trim();
	}
	// get form noun,for example PLURAL_NOMINATIVE
	// (миллионов,тысяч,миллиардов),SINGULAR_GENITIVE(миллиона,миллиарда) и
	// SINGULAR_NOMINATIVE (миллион,миллиард)

	private static FormNoun getFormNounNumberThreeDigit(short number) {

		int[] digits = getDigitsNumberThreeDigit(number);
		if (digits[1] == 1)
			return FormNoun.PLURAL_NOMINATIVE;
		if (digits[2] == 1)
			return FormNoun.SINGULAR_NOMINATIVE;
		if (digits[2] > 1 && digits[2] < 5)
			return FormNoun.SINGULAR_GENITIVE;
		return FormNoun.PLURAL_NOMINATIVE;
	}

	// decomposition of a number into groups of max three-digit numbers , 1 123 324
	// => [0,0,0,123,324];105 123 324 => [0,0,105,123,324]

	private static short[] decompNumberInGroupThreeDigit(long number) {
		long digit = TRILLION;// start from trillion
		long number1 = number;
		short[] res_decomposition = new short[5];
		for (byte i = 0; i < 5; i++) {
			res_decomposition[i] = (short) (number1 / digit);
			number1 = number1 - res_decomposition[i] * digit;
			digit = digit / THOUSAND;
		}
		return res_decomposition;
	}

	// Convert to words
	public static String convertToWords(long number)

	{
		StringBuilder result = new StringBuilder(1000);

		if (number == 0)
			return "ноль";

		long number_positive = number;
		if (number < 0) {
			number_positive = -number;
			result.append("минус ");
		}

		short groups[] = decompNumberInGroupThreeDigit(number_positive);

		for (int i = 0; i < 5; i++) {
			if (groups[i] > 0) {
				FormNoun formNounNumber = getFormNounNumberThreeDigit(groups[i]);

				if (i == 4) {
					result.append(convertNumberThreeDigitInWords(groups[i], Gender.MALE));
				}
				if (i == 3) {
					result.append(convertNumberThreeDigitInWords(groups[i], Gender.FEMALE)).append(" ")
							.append(nameThousand[formNounNumber.ordinal()]);
				}
				if (i == 2) {
					result.append(convertNumberThreeDigitInWords(groups[i], Gender.MALE)).append(" ")
							.append(nameMillion[formNounNumber.ordinal()]);
				}
				if (i == 1) {
					result.append(convertNumberThreeDigitInWords(groups[i], Gender.MALE)).append(" ")
							.append(nameBillion[formNounNumber.ordinal()]);
				}
				if (i == 0) {
					result.append(convertNumberThreeDigitInWords(groups[i], Gender.MALE)).append(" ")
							.append(nameTrillion[formNounNumber.ordinal()]);
				}
				result.append(" ");
			}

		}

		return result.toString().trim();
	}

}
