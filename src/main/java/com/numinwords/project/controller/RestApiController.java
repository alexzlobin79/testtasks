package com.numinwords.project.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.numinwords.project.api.IRoutingApi;
import com.numinwords.project.model.Words;
import com.numinwords.project.service.NumWordsService;

@RestController
@RequestMapping(IRoutingApi.MAIN)
public class RestApiController implements IRoutingApi {
	@RequestMapping(value = IRoutingApi.GETNUMINWORDS + "/{number}", method = RequestMethod.GET)
	public Words getWords(@PathVariable(value = "number") long number) {
		return NumWordsService.getWordsfromNumber(number);
	}
}