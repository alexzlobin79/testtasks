package com.numinwords.project.api;

public interface INumberSystem {
	String DECIMAL = "10";
	String OCTAL = "8";
}
